TARGETS = postgresql.group.json projects archived-projects

all: $(TARGETS)

postgresql.group.json:
	curl -f https://salsa.debian.org/api/v4/groups/postgresql | jq . > $@

projects: postgresql.group.json Makefile
	jq '.projects[] | select(.archived == false) | .name' $< | \
		tr -d '"' | sort> $@

archived-projects: postgresql.group.json Makefile
	jq '.projects[] | select(.archived) | .name' $< | \
		tr -d '"' | sort> $@

clean:
	rm -f $(TARGETS)
